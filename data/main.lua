--require"scripts/features"
require"scripts/multi_events"
local game_manager = require"scripts/game_manager"

-- This function is called when Solarus starts.
function sol.main:on_started()
	sol.language.set_language"en"
	sol.video.set_window_title("Sample quest - Solarus " .. sol.main.get_solarus_version())
	sol.video.set_window_size(sol.video.get_quest_size()) --don't upscale window size
	
	sol.main.load_settings()
	math.randomseed(os.time())
	
	local game = game_manager:create"save1.dat"
	game:start()
end

--Event called when the program stops.
function sol.main:on_finished()
  sol.main.save_settings()
end

--Event called when the player pressed a keyboard key.
function sol.main:on_key_pressed(key, modifiers)
	local handled = false
	if key == "return" and (modifiers.alt or modifiers.control) then
		--Ctrl + return or Alt + Return: switch fullscreen.
		sol.video.set_fullscreen(not sol.video.is_fullscreen())
		handled = true
	elseif key == "f4" and modifiers.alt then
		-- Alt + F4: stop the program.
		sol.main.exit()
		handled = true
	end
	
	return handled
end
