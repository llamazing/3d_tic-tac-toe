local board_manager = require"scripts/board"
local npc_manager = require"scripts/npc"

local menu = {}

local X_OFFSET = 32
local Y_OFFSET = 20

local COLORS = {
	{ --purple
		{191, 115, 229},
		{150, 41, 204},
	},
	{ --yellow
		{255, 255, 128},
		{203, 203, 41},
	},
	{204, 41, 41}, --red
}

--convenience
local math__floor = math.floor

local board_img = sol.surface.create"board.png"
local spot_sprites = {}

local board = nil
local npc = nil
local current_player = nil
local prev_spot = nil

local function xy2spot(x, y)
	x = x - X_OFFSET - 1 --left edge is 2px wide, ignore outer-most pixel
	y = y - Y_OFFSET
	
	for n=0,4 do --board panel number clicked
		if y >= n*100+0 and y <= n*100+94 then
			local y_relative = y - n*100
			local x_adjusted = x - (94 - y_relative)
			
			if x_adjusted >= 0 and x_adjusted < 130 then
				local row_num = math__floor(y_relative/19)
				local col_num = math__floor(x_adjusted/26)
				local spot = col_num + 5*row_num + 25*n + 1
				
				return spot
			else return nil end --y coord won't match any additional panels so don't bother checking
		end
	end
	
	return nil
end

local function spot2xy(spot)
	spot = tonumber(spot)
	assert(spot, "Bad argument #1 to 'spot2xy' (number expected)")
	spot = math__floor(spot)
	assert(spot >= 1 and spot <= 125, "Bad argument #1 to 'spot2xy' (number value must be 1-125)")
	
	local n = math__floor((spot - 1)/25)
	local row_num = math__floor((spot - n*25 - 1)/5) % 5
	local col_num = (spot - 1) % 5
	
	local y_relative = row_num*19 + 10
	local x_adjusted = col_num*26 + 14
	
	local x = x_adjusted + 94 - y_relative + X_OFFSET + 1
	local y = y_relative + n*100 + Y_OFFSET
	
	return x,y
end

--create sprites for each spot
for spot=1,125 do
	local sprite = sol.sprite.create"spots"
	sprite:set_xy(spot2xy(spot))
	spot_sprites[spot] = sprite
end

	--owner
		--nil - unassigned
		--0 - belongs to all players (free spot)
		--1 - player 1
		--2 - player 2
local function set_spot_owner(spot, owner)
	local sprite = spot_sprites[spot]
	assert(sprite, "Bad argument #1 to 'set_spot_owner', invalid spot id:"..tostring(spot))
	if not owner then
		sprite:set_animation"white"
	elseif owner==0 then
		sprite:set_animation"free_spot"
	else
		sprite:set_animation"white"
		sprite:set_color_modulation(COLORS[owner][2])
		
		if prev_spot then
			local prev_sprite = spot_sprites[prev_spot]
			local prev_owner = board:get_spot_owner(prev_spot)
			prev_sprite:set_color_modulation(COLORS[prev_owner][1])
		end
		prev_spot = spot
	end
end

local function new_game()
	board = board_manager.new()
	npc = npc_manager.new(board, 2)
	
	--place free spot in center of board, available to both players
	set_spot_owner(63, 0)
	board:make_move(63, 0)
	
	current_player = 1
end

local function highlight_row(row)
	local game = sol.main.get_game()
	
	for _,spot in ipairs(row) do
		local sprite = spot_sprites[spot]
		sprite.orig_color = sprite:get_color_modulation()
		sprite:set_color_modulation(COLORS[3])
		sprite.is_highlighted = true
		
		sol.timer.start(game, 500, function()
			if sprite.is_highlighted then
				sprite:set_color_modulation(sprite.orig_color)
			else sprite:set_color_modulation(COLORS[3]) end
			
			sprite.is_highlighted = not sprite.is_highlighted
			
			return true
		end)
	end
end

local function place_spot(spot)
	--player made move
	if not board:is_spot_empty(spot) then return end --invalid move
	
	set_spot_owner(spot, current_player)
	is_done, winning_row, winning_player = board:make_move(spot, current_player)
	if is_done then
		current_player = false
		print("Game Over:", is_done, "Winner:", tostring(winning_player))
		if winning_row then highlight_row(winning_row) end
		return
	end
	current_player = 3 - current_player
	
	--now have npc make move
	spot = npc:go()
	set_spot_owner(spot, current_player)
	is_done, winning_row, winning_player = board:make_move(spot, current_player)
	if is_done then
		current_player = false
		print("Game Over:", is_done, "Winner:", tostring(winning_player))
		if winning_row then highlight_row(winning_row) end
		return
	end
	current_player = 3 - current_player
end

function menu:on_started()
	new_game()
end

function menu:on_mouse_pressed(button, x, y)
	if current_player ~= 1 then return false end --block player inputs during npc turn
	
	local spot = xy2spot(x, y)
	if spot then
		place_spot(spot)
		return true
	else return false end
end

function menu:on_draw(dst)
	for i=0,4 do
		board_img:draw(dst, X_OFFSET, i*100 + Y_OFFSET)
	end
	for _,sprite in ipairs(spot_sprites) do
		sprite:draw(dst)
	end
end

return menu
