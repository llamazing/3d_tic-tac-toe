local npc_manager = {}

function npc_manager.new(board, ME)
	local FOE = 3 - ME
	local RANKINGS = {
		[0] = 2,
		[0.1] = 4, --unowned row with 1 free spot
		[ME + 0.1] = 3,
		[ME + 0.2] = 6,
		[ME + 0.3] = 9,
		[FOE + 0.1] = 0,
		[FOE + 0.2] = 1,
		[FOE + 0.3] = 5,
	}
	
	local npc = {}
	
	--convenience
	local rows_with_spot = board.rows_with_spot
	local empty_spots = board.empty_spots
	local my_rows = board.rows_by_owner[ME]
	local foe_rows = board.rows_by_owner[FOE]
	local spot_counts_by_row = board.spot_counts_by_row
	
	--// npc makes next move
		--returns the empty spot (number, positive integer) to select for this move
	function  npc:go()
		--check if any spots give win
		for row in pairs(my_rows) do
			if spot_counts_by_row[row]==4 then
				--find empty spot in row
				for _,spot in ipairs(row) do
					if empty_spots[spot] then return spot end
				end
			end
		end
		
		--check if any spots would give opponent win
		for row in pairs(foe_rows) do
			if spot_counts_by_row[row]==4 then
				--find empty spot in row
				for _,spot in ipairs(row) do
					if empty_spots[spot] then return spot end
				end
			end
		end
		
		--check if any spots ensure victory
		do
			local cross_spots = {}
			for row in pairs(my_rows) do
				if spot_counts_by_row[row]==3 then
					for _,spot in ipairs(row) do
						if empty_spots[spot] then
							if cross_spots[spot] then --this is the 2nd row found with this spot
								return spot --will now own 4 spots in two different rows; victory guaranteed
							else cross_spots[spot] = true end
						end
					end
				end
			end
		end
		
		--check if any spots ensure victory for opponent
		do
			local cross_spots = {}
			for row in pairs(foe_rows) do
				if spot_counts_by_row[row]==3 then
					for _,spot in ipairs(row) do
						if empty_spots[spot] then
							if cross_spots[spot] then --this is the 2nd row found with this spot
								return spot --block opponent from owning 4 spots in two separate rows
							else cross_spots[spot] = true end
						end
					end
				end
			end
		end
		
		--iterate thru all empty spots and choose one with highest ranking
		local best_rank, best_spot = 0
		for spot in pairs(empty_spots) do
			local rank = 0
			for _,row in ipairs(rows_with_spot[spot]) do
				local owner = board:get_row_owner(row)
				if owner ~= 0 then --is non-contested
					local owner_count = spot_counts_by_row[row] or 0
					local lookup_code = (owner or 0) + owner_count/10
					rank = rank + (RANKINGS[lookup_code] or 0)
					--if spot==34 then print("rank code", lookup_code, ME, FOE) end
				end
			end
			
			--save reference if best spot found so far
			--print(spot, rank)
			if rank > best_rank then
				best_rank = rank
				best_spot = spot
			end
		end
		
		if best_spot then return best_spot end
		
		print"uh oh!"
		return false --shouldn't happen
	end
	
	return npc
end

return npc_manager
		