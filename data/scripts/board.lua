local ALL_ROWS = require"scripts/all_rows"
local ROWS_FROM_SPOT = {}
for i,list in ipairs(ALL_ROWS) do
	for n,spot in ipairs(list) do
		if not ROWS_FROM_SPOT[spot] then
			ROWS_FROM_SPOT[spot] = {list}
		else table.insert(ROWS_FROM_SPOT[spot], list) end
	end
end

local rows_with_spot = {}
for i,row in ipairs(ALL_ROWS) do
	for n,spot in ipairs(row) do
		if not rows_with_spot[spot] then
			rows_with_spot[spot] = {row}
		else table.insert(rows_with_spot[spot], row) end
	end
end

--convenience
local math__floor = math.floor

local board_manager = {}

function board_manager.new()
	--start with all spots empty
	local empty_spots = {}
	for i=1,125 do empty_spots[i] = true end
	
	--no spots or rows owned yet
	local row_owner_lookup = {}
	local spot_owner_lookup = {}
	local rows_by_owner = { {}, {}, }
	local spot_counts_by_row = {}
	local row_counts = {
		[0]=109, [1]=0, [2]=0,
	}
	
	local board = {
		rows_with_spot = rows_with_spot,
		rows_by_owner = rows_by_owner,
		empty_spots = empty_spots,
		spot_counts_by_row = spot_counts_by_row,
	}
	
	--// Returns the owner (number, non-negative integer or nil) of the given row (table)
		--returns
			--nil - row is not yet taken
			--0 - a dead row with spots owned by both players
			--1 - row only contains spots owned by player 1
			--2 - row only contains spots owned by player 2
	function board:get_row_owner(row)
		return row_owner_lookup[row]
	end
	
	--// Returns the owner (number, non-negative integer or nil) of the given spot (number, positive integer)
		--returns
			--nil - spot is unoccupied
			--0 - a free spot belonging to all players
			--1 - spot belongs to player 1
			--2 - spot belongs to player 2
	function board:get_spot_owner(spot)
		return spot_owner_lookup[spot]
	end
	
	function board:is_spot_empty(spot)
		return empty_spots[spot]
	end
	
	function board:make_move(spot, player)
		spot = tonumber(spot)
		assert(spot, "Bad argument #1 to 'make_move' (number expected)")
		spot = math__floor(spot)
		assert(spot>=1 and spot<=125, "Bad argument #1 to 'make_move', invalid spot: "..spot)
		assert(empty_spots[spot], "Bad argument #1 to 'make_move' (spot is not empty)")
		
		player = tonumber(player)
		assert(player, "Bad argument #2 to 'make_move' (number expected)")
		player = math__floor(player)
		assert(player>=0 and player<=2, "Bad argument #2 to 'make_move', invalid player: "..player)
		
		local is_win = false --tentative
		for _,row in ipairs(rows_with_spot[spot]) do
			if player > 0 then
				local prev_owner = row_owner_lookup[row]
				if not prev_owner then
					--this previously unowned row is now owned
					rows_by_owner[player][row] = true
					row_counts[0] = row_counts[0] - 1
					row_counts[player] = row_counts[player] + 1
					row_owner_lookup[row] = player --now owned
				elseif prev_owner>0 and prev_owner ~= player then
					--this row is now dead (owned by both players)
					rows_by_owner[prev_owner][row] = nil --remove from previous owner
					row_counts[prev_owner] = row_counts[prev_owner] - 1
					row_owner_lookup[row] = 0 --dead
				end
			end
			
			spot_counts_by_row[row] = (spot_counts_by_row[row] or 0) + 1
			if spot_counts_by_row[row] == 5 and row_owner_lookup[row]==player then
				is_win = row
			end
		end
		spot_owner_lookup[spot] = player
		empty_spots[spot] = nil
		
		if is_win then
			local row = is_win
			return "win", row, player
		elseif row_counts[0]==0 and row_counts[1]==0 and row_counts[2]==0 then --stalemate
			return "stalemate"
		end
		
		return false
	end
	
	return board
end

return board_manager
