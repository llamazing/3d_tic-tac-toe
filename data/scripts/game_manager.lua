--[[
	Usage:
		local game_manager = require"scripts/game_manager"
		local game = game_manager:create"savegame_file_name"
		game:start()
]]

require"scripts/multi_events"
local game_board = require"scripts/menus/game_board"
--local initial_game = require"scripts/initial_game"

local game_manager = {}

function game_manager:create(file)
	local exists = sol.game.exists(file)
	local game = sol.game.load(file)
	if not exists then
		--initial_game:initialize_new_savegame(game)
	end
	
	function game:on_started()
		sol.menu.start(self, game_board)
	end
	
	return game
end



return game_manager
